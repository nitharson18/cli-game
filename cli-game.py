#!/usr/bin/env python3.10

import sys
import argparse
from typing import Protocol
from dataclasses import dataclass


class Person(Protocol):
    pass


@dataclass
class Warrior(Person):
    """Warrior class"""
    pass


@dataclass
class Hunter(Person):
    """Hunter class"""
    pass


@dataclass
class Wizard(Person):
    """Wizard class"""
    pass


@dataclass
class Orc(Person):
    """Orc class"""
    pass


@dataclass
class Goblin(Person):
    """Goblin class"""
    pass


@dataclass
class Zombie(Person):
    """Zombie class"""
    pass


class TeamIterator:

    def __init__(self, team):
        pass

    def __next__(self):
        pass


class Team:

    def __init__(self, members):
        pass

    def __len__(self):
        pass

    def __getitem__(self, index):
        pass

    def __iter__(self):
        pass
            

class EnemyTeam(Team):
    
    def __init__(self, unit, nb_troops):
        pass

    @property
    def damage(self):
        pass

    @property
    def loot(self):
        pass

    @property
    def unit(self):
        pass


class PlayerTeam(Team):
 
    def __init__(self, nb_warriors, nb_hunters, nb_wizards):
        pass

    @property
    def damage(self):
        pass

    @property
    def luck(self):
        pass

    @property
    def flee(self):
        pass

    @property
    def nb_warriors(self):
        pass

    @property
    def nb_hunters(self):
        pass

    @property
    def nb_wizards(self):
        pass

    def __repr__(self):
        pass


class GameException(Exception):
    pass


class Game:
    
    history_file = "game_data.json"
    
    def __init__(self):
        res = input("entrez votre pseudo")
        print("votre pseudo est")
        print(res)
        pass

    def config(self):
        pass

    def status(self):
        pass

    def __load_player_team(self):
        pass

    def __load_enemy_team(self):
        pass

    def player_damage(self):
        pass

    def enemy_damage(self):
        pass

    def load_game(self):
        pass

    def start_game(self):
        pass

    def buy(self, person):
        pass

    def move(self, direction):
        pass

    def fight(self):
        pass
        
    def flee(self):
        pass


@dataclass
class Loses:
    """Loses class"""
    pass


def make_argparser():
    parser = argparse.ArgumentParser(description='Game Command Ligne Interface.')

    parser.add_argument('--config', action='store_true', default=False,
                        help='run game configuration')

    parser.add_argument('--start', action='store_true', default=False,
                        help='start a new game')

    parser.add_argument('--status', action='store_true', default=False,
                        help='show current game status')

    parser.add_argument('--buy', '-b', dest='buy', type=str,
                        choices=['Warrior', 'Hunter', 'Wizard'],
                        help='buy new team member')

    parser.add_argument('--move', '-m', dest='move', type=str,
                        choices=['North', 'South', 'East', 'West'],
                        help='move in some direction')

    parser.add_argument('--fight', action='store_true', default=False,
                        help='fight your enemies')

    parser.add_argument('--flee', action='store_true', default=False,
                        help='run away')
    return parser

def main(args):
    """Main function
    """
    parser = make_argparser()
    opts = parser.parse_args(args)

    game = Game()

    if opts.config:
        game.config()
    elif opts.start:
        game.start_game()
    elif opts.status:
        game.status()
    elif opts.buy:
        game.buy(opts.buy)
    elif opts.move:
        game.move(opts.move)
    elif opts.fight:
        game.fight()
    elif opts.flee:
        game.flee()

if __name__ == "__main__":
    ret = main(sys.argv[1:])
    sys.exit(ret)
